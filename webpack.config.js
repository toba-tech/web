const source = './';
const target = './static';
const webpack = require('webpack');
const path = require('path');
const env = process.env.NODE_ENV || 'development';
const isProd = env === 'production';
const { CheckerPlugin, TsConfigPathsPlugin } = require('awesome-typescript-loader');
const context = path.join(__dirname, source);

module.exports = {
   entry: {
      home: "./src/home.ts",
      product: "./src/product.ts"
   },
   output: {
      path: path.join(__dirname, target),
      filename: 'js/[name].js',
      publicPath: '/static/'
   },

   module: {
      rules: [
         // Pass TypeScript to its loader
         // https://www.typescriptlang.org/docs/handbook/react-&-webpack.html
         // https://github.com/TypeStrong/ts-loader/issues/227
         {
            test: /\.tsx?$/,
            exclude: [/node_modules/, /test\.tsx?$/, /src\/types\//],
            loader: ['awesome-typescript-loader']
         }
      ]
   },
   resolve: {
      extensions: [".ts", ".tsx", ".json"],
      plugins: [new TsConfigPathsPlugin()]
   },
   plugins: [
      new CheckerPlugin(),

      new webpack.LoaderOptionsPlugin({
         minimize: true,
         debug: false,
      }),

      // https://medium.com/webpack/webpack-3-official-release-15fd2dd8f07b
      // https://medium.com/webpack/webpack-freelancing-log-book-week-5-7-4764be3266f5
      new webpack.optimize.ModuleConcatenationPlugin(),

      // https://github.com/webpack/webpack/issues/2431
      // https://webpack.github.io/docs/list-of-plugins.html#sourcemapdevtoolplugin
      // https://github.com/AngularClass/angular2-webpack-starter/issues/144
      new webpack.SourceMapDevToolPlugin({
         test: [/\.js$/],
         exclude: [/common\./],
         filename: 'js/[name].js.map',
      }),

      // Minify Javascript but only mangle it for production
      // http://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
      new webpack.optimize.UglifyJsPlugin({
         compress: {
            warnings: false,
            screw_ie8: true
         },
         // mangled variables are trouble for source maps
         mangle: true,
         output: { comments: false },
         sourceMap: true
      })
   ]
};