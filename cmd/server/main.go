// https://echo.labstack.com/guide
package main

import (
	"errors"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/nicksnyder/go-i18n/i18n"
	"toba.tech/web/lib/html"
)

type (
	Template struct {
		templates *template.Template
	}

	// ViewData are sent to each template. Products are used to build the product
	// menu and the Group indicates which Javascript and Stylesheet should be
	// loaded.
	ViewData struct {
		Products map[string]*Product
		Group    string
	}

	Product struct {
		Name      string
		Free      bool
		Available bool
	}

	echoHandler interface {
		GET(string, echo.HandlerFunc, ...echo.MiddlewareFunc) *echo.Route
	}
)

var (
	// productPaths match product URL path to its name and whether it's free.
	// Free products get a visual cue.
	productPaths = map[string]*Product{
		"service-desk":           &Product{"Service Desk", false, true},
		"incident-management":    &Product{"Incident Management", false, true},
		"configuration-database": &Product{"Configuration Database", false, true},
		"task-board":             &Product{"Task Board", true, true},
		"chat":                   &Product{"Group Chat", true, true},
	}
	rootPaths = []string{"contact", "about", "privacy", "download", "technology"}
	rootView  = &ViewData{
		Products: productPaths,
		Group:    "home",
	}
	productView = &ViewData{
		Products: productPaths,
		Group:    "product",
	}
)

// Template i18n example:
// https://github.com/nicksnyder/go-i18n/blob/master/i18n/exampletemplate_test.go
func main() {
	e := echo.New()

	i18n.MustLoadTranslationFile("i18n/en-US.all.json")
	T, _ := i18n.Tfunc("en-US")

	temp := template.New("main").Funcs(map[string]interface{}{
		"T":       T,
		"icon":    html.Icon,
		"iconCSS": html.IconCSS,
	})

	err := filepath.Walk("views", func(path string, info os.FileInfo, err error) error {
		if strings.HasSuffix(path, ".html") {
			_, err := temp.ParseFiles(path)
			if err != nil {
				return err
			}
		}
		return nil
	})

	if err != nil {
		e.Logger.Fatal(err)
	}

	err = filepath.Walk(path.Join("static", "img"), func(path string, info os.FileInfo, err error) error {
		if strings.HasSuffix(path, ".svg") {
			_, err := temp.ParseFiles(path)
			if err != nil {
				return err
			}
		}
		return nil
	})

	if err != nil {
		e.Logger.Fatal(err)
	}

	t := &Template{templates: temp}

	// e.AutoTLSManager.HostPolicy = func(ctx context.Context, host string) error {
	// 	if !strings.HasSuffix(host, "toba.tech") {
	// 		return errors.New("invalid domain")
	// 	}
	// 	return nil
	// }

	// e.AutoTLSManager.Cache = autocert.DirCache("./certs")

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Renderer = t
	e.Static("/", "static")

	e.GET("/", func(c echo.Context) error {
		return c.Render(http.StatusOK, "home", rootView)
	})

	for _, path := range rootPaths {
		handlePath(e, path, rootView)
	}

	products := e.Group("/product")

	for path, _ := range productPaths {
		if t.templates.Lookup(path+".html") == nil {
			e.Logger.Fatal(errors.New("Template " + path + " does not exist"))
		}
		handlePath(products, path, productView)
	}

	// articles := e.Group("/article")

	// for path, _ := range productPaths {
	// 	if t.templates.Lookup(path+".html") == nil {
	// 		e.Logger.Fatal(errors.New("Template " + path + " does not exist"))
	// 	}
	// 	handlePath(products, path, productView)
	// }

	//e.Logger.Fatal(e.StartAutoTLS(":4433"))

	port := os.Getenv("PORT")
	if port == "" {
		port = "3000"
	}

	e.Logger.Fatal(e.Start(":" + port))
}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name+".html", data)
}

// handlePath matches path to a route and renderer. When used in a range
// iteration, this function call prevents the path reference from being
// reassigned to the final iteration value before rendering.
func handlePath(e echoHandler, path string, data interface{}) {
	e.GET("/"+path+"/", func(c echo.Context) error {
		log.Printf("Rendering %s for %s", path, c.Path())
		return c.Render(http.StatusOK, path, data)
	})
}
