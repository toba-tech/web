
window.addEventListener("DOMContentLoaded", ()=> {
   const menuItems = document.querySelectorAll("body > nav > ul.menu > li > ul > li");

   for (let i = 0; i < menuItems.length; i++) {
      const li = menuItems[i] as HTMLElement;
      const url = li.dataset["href"];
      if (url !== undefined) {
         li.addEventListener("click", (e:Event)=> {
            e.preventDefault();
            window.location.href = url;
         });
      }
   }
});