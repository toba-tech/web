/**
 * Pixel distance from frog at which cursor triggers eye movement.
 */
const threshold = 300;
let frogs:Frog[] = [];

window.addEventListener("DOMContentLoaded", ()=> {
   const matches = document.querySelectorAll("svg.frog") as NodeListOf<SVGElement>;
   if (matches === null) { return; }
   for (let i = 0; i < matches.length; i++) {
      const f = new Frog(matches[i]);
      if (f.valid) { frogs.push(f); }
   }
   if (frogs.length > 0) {
      document.addEventListener("mousemove", (e:MouseEvent) => {
         frogs.forEach(f => f.handleMouseMove(e));
      });
      frogs.forEach(f => f.handleClick());
   }
});

class Frog {
   svg:SVGElement;
   tongue:NodeListOf<SVGPathElement>;
   eyes:Eye[];
   /** Ratio of eye movement to cursor distance. */
   ratio:number;
   /** Page-relative x,y coordaintes of logo center. */
   center:number[];
   valid = false;
   /** Whether frog is watching cursor. */
   watching = false;
   /** Reset eye position timer. */
   timer:number;

   constructor(el:SVGElement) {
      this.svg = el;
      const rect = el.getClientRects()[0];
      const pupils = el.querySelectorAll(".pupil") as NodeListOf<SVGPathElement>;
      const eyes = el.querySelectorAll(".eyes") as NodeListOf<SVGEllipseElement>;
      this.tongue = el.querySelectorAll(".tongue") as NodeListOf<SVGPathElement>;
      this.valid = pupils !== null && eyes !== null && this.tongue !== null;

      if (!this.valid) { return; }

      this.center = [rect.left + rect.width / 2, rect.top + rect.height / 2];
      this.eyes = [
         new Eye(eyes[0], pupils[0], 8),
         new Eye(eyes[1], pupils[1], -8)
      ];
   }

   /**
    * Animate eyes to look at cursor when it's nearby and to stretch tongue to
    * cusor when logo is clicked.
    *
    * Coordinates begin at 0,0 in upper left.
    */
   handleMouseMove(e:MouseEvent) {
      const frog = this;

      if (Math.abs(e.clientX - this.center[0]) > threshold ||
          Math.abs(e.clientY - this.center[1]) > threshold) {

         if (this.watching) {
            // reset eyes if they were watching the cursor
            frog.timer = window.setTimeout(()=> {
               frog.eyes[0].reset();
               frog.eyes[1].reset();
            }, 200);
            this.watching = false;
         }
         return;
      }
      if (frog.timer) { window.clearTimeout(frog.timer); }
      frog.eyes[0].move(e);
      frog.eyes[1].move(e);
      this.watching = true;
   }

   handleClick() {
      const frog = this;
      this.svg.addEventListener("click", (e:MouseEvent) => {
         e.preventDefault();
         frog.tongue[0].style.transform = "scaleY(1)";
         frog.tongue[1].style.transform = "scaleY(1)";
         window.setTimeout(()=> {
            frog.tongue[0].style.transform = null;
            frog.tongue[1].style.transform = null;
         }, 100);
      });
   }
}

class Eye {
   pupil:SVGPathElement;
   pupilX:number;
   pupilY:number;
   ratioX:number;
   ratioY:number;
   rotate:number;
   maxDistance:number;

   constructor(eye:SVGEllipseElement, pupil:SVGPathElement, rotate:number) {
      this.pupil = pupil;
      this.rotate = rotate;

      const rect = pupil.getClientRects()[0];
      const eyeSize = eye.getClientRects()[0].width - 5;
      const maxX = (eyeSize - rect.width) / 2;
      const maxY = (eyeSize - rect.height) / 2;

      this.maxDistance = eyeSize;

      this.ratioX = maxX / eyeSize;
      this.ratioY = maxY / eyeSize;
      this.pupilX = rect.left + window.scrollX + rect.width / 2;
      this.pupilY = rect.top + window.scrollY - 80 + rect.height / 2;
   }

   move(e:MouseEvent) {
      const dx = this.dist(e.pageX, this.pupilX, this.ratioX);
      const dy = this.dist(e.pageY, this.pupilY, this.ratioY);
      this.pupil.style.transform = `translate(${dx}px, ${dy}px) rotate(${this.rotate}deg)`;
   }

   dist(m:number, c:number, r:number) {
      let d = m - c;
      if (Math.abs(d) > this.maxDistance) {
         d = this.maxDistance * (d < 0 ? -1 : 1);
      }
      return d * r;
   }

   reset() {
      this.pupil.style.transform = null;
   }
}