import "./menu";
import "./logo";

window.addEventListener("DOMContentLoaded", showHighResHero);

/**
 * Replace initial low-resolution hero image with high-resolution version.
 * Assign source to temporary image element to ensure it's fully loaded before
 * making it the hero image.
 */
function showHighResHero():void {
   const hero = document.querySelector(".hero img") as HTMLImageElement;
   if (hero == null) { return; }

   const url = hero.src.replace("_small", "");
   const img = document.createElement("img");

   img.addEventListener("load", ()=> { hero.src = url; });
   img.src = url;
}