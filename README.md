# Dependencies
`go get`
```
github.com/labstack/echo/
github.com/nicksnyder/go-i18n/goi18n
github.com/codegangsta/gin
```

# Developent

Add SVG Cleaner to path


## OSX
```
brew install heroku
brew install wellington
```

# Deployment
Uses binary Heroku buildpack
https://github.com/ph3nx/heroku-binary-buildpack
https://github.com/ph3nx/heroku-binary-buildpack.git

add `PATH` env
```
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/app/bin
```


SCSS

- Download and install `tdm64-gcc-5.1.0-2.exe` from http://tdm-gcc.tdragon.net/download.
- Go to Program Files and click on "MinGW Command Prompt". This will open a console with the correct environment for using MinGW with GCC.
- Within this console, navigate to your GOPATH.
- Enter the following commands:
```
go get -u github.com/wellington/spritewell
go get -u github.com/wellington/wellington
go install github.com/wellington/wellington/wt
```
- then you can do:
```
wt compile static/scss/mystuff.scss -s compressed -b static/css
```

https://github.com/sass/libsass/blob/master/docs/build-on-windows.md#building-via-minggw-64bit-makefiles

# Hero Images
2048x1152 large (16:9)
Use `Job Identifier` IPTC field to name

# Fonts
## Sans serif
Viga
Yanone Kaffeesatz
Homenaje

## Serif