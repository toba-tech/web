package html

import "html/template"

func Icon(name string) template.HTML {
	return IconCSS(name, "")
}

func IconCSS(name, css string) template.HTML {
	if len(css) > 0 {
		css += " "
	}
	css += "material-icons"
	return template.HTML("<i class=\"" + css + "\">" + name + "</i>")
}
